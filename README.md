# Hello World CI/CD

This repository contains all code to create a hello world service based on flask. 
gitlab CI/CD services are leveraged to perform unit tests and build a docker image 
which is pushed to a repository.  Finally, the docker image is [deployed](http://supernode.leggewie.org:5000) to a remote 
server via ssh.
