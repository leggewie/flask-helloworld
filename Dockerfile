FROM python:3.8-alpine

MAINTAINER Rolf Leggewie <foss@leggewie.org>

# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

WORKDIR /app
ENTRYPOINT [ "gunicorn" ]
CMD [ "helloworld:app", "-b 0.0.0.0:5000", "-w 2" ]
